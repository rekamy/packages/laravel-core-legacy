<?php

namespace Rekamy\LaravelCoreLegacy\Override;

use Illuminate\Pagination\LengthAwarePaginator as BaseLengthAwarePaginator;

class LengthAwarePaginator extends BaseLengthAwarePaginator
{
    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'data' => $this->items->toArray(),
            'total' => $this->total(),
            'draw' => (int) request()->get('draw'),
            'from' => $this->firstItem(),
            'to' => $this->lastItem(),
            'current_page' => $this->currentPage(),
            'last_page' => $this->lastPage(),
            'first_page_url' => $this->url(1),
            'last_page_url' => $this->url($this->lastPage()),
            'next_page_url' => $this->nextPageUrl(),
            'path' => $this->path(),
            'links' => $this->linkCollection()->toArray(),
            'per_page' => $this->perPage(),
            'prev_page_url' => $this->previousPageUrl(),
            'recordsTotal' => $this->items->count(),
            'recordsFiltered' => $this->total(),
        ];
    }

    /**
     * Get the current page for the request.
     *
     * @param  int  $currentPage
     * @param  string  $pageName
     * @return int
     */
    protected function setCurrentPage($currentPage, $pageName)
    {
        $currentPage = $currentPage ?: static::resolveCurrentPage('start');

        return $this->isValidPageNumber($currentPage) ? (int) $currentPage : 1;
    }
}
