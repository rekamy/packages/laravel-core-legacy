<?php

namespace Rekamy\LaravelCoreLegacy\Crudable\Concern;

use Rekamy\LaravelCoreLegacy\Contracts\CrudableRequest;

trait HasRequest
{
    public function registerRequest(CrudableRequest $request)
    {
        $this->request = $request;
    }

    public function getRequest()
    {
        return $this->request;
    }

}
