<?php

namespace Rekamy\LaravelCoreLegacy\Crudable\Abstract;

use Rekamy\LaravelCoreLegacy\Contracts\CrudableBloc as CrudableBlocInterface;
use Rekamy\LaravelCoreLegacy\Crudable\Concern\CrudableBloc;
use Rekamy\LaravelCoreLegacy\Crudable\Concern\HasRepository;
use Rekamy\LaravelCoreLegacy\Crudable\Concern\HasRequest;

abstract class CrudBloc implements CrudableBlocInterface
{
    use HasRepository, HasRequest, CrudableBloc;

    protected $moduleName;

}
